syntax enable
filetype plugin indent on

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

"look
Plugin 'mhinz/vim-startify'
Plugin 'airblade/vim-gitgutter'
Plugin 'morhetz/gruvbox'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

"feel
Plugin 'honza/vim-snippets'
Plugin 'mattn/emmet-vim'
Plugin 'airblade/vim-rooter'

" utils
Plugin 'tmhedberg/SimpylFold'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-surround'

" searching
Plugin 'haya14busa/incsearch.vim'
Plugin 'haya14busa/incsearch-fuzzy.vim'
Plugin 'haya14busa/incsearch-easymotion.vim'
Plugin 'chaoren/vim-wordmotion'
Plugin 'easymotion/vim-easymotion'
Plugin 'mg979/vim-visual-multi'
Plugin 'yggdroot/leaderf'
Plugin 'junegunn/fzf.vim'

" formatting
Plugin 'majutsushi/tagbar'
Plugin 'tpope/vim-commentary'
Plugin 'godlygeek/tabular'

" trees
Plugin 'vim-ctrlspace/vim-ctrlspace'
Plugin 'mbbill/undotree'
Plugin 'ms-jpq/chadtree'

"language support
Plugin 'whonore/Coqtail'
Plugin 'sheerun/vim-polyglot'
Plugin 'alunny/pegjs-vim'
Plugin 'vigoux/LanguageTool.nvim'

"for arduino/esp32
" Plugin 'coddingtonbear/neomake-platformio'
" Plugin 'stevearc/vim-arduino'
call vundle#end()
