set nu
set relativenumber
set termguicolors
set clipboard=unnamedplus

" required for C-Space
set hidden

" don't show eg. "--INSERT--" since arirline already does it
set noshowmode

" show hidden characters
set list
set lcs=tab:▒░,trail:▓,nbsp:░
set noexpandtab

set tabstop=4
set shiftwidth=4
set noexpandtab

colorscheme gruvbox

let g:python3_host_prog = '/usr/bin/python'

let g:airline_powerline_fonts = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_tabs = 0
let g:airline#extensions#tabline#buffers_label = ""
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_exclude_preview = 0
let g:airline_theme = 'base16_gruvbox_dark_hard'
let g:airline_section_b = ""
let g:airline_section_x = ""
let g:airline_section_y = ""
let g:airline_section_z = "%l/%L"

let mapleader = "`"

let g:gitgutter_terminal_reports_focus=0
let g:CtrlSpaceDefaultMappingKey = "<C-space> "

let g:Lf_PreviewInPopup = 1
let g:tex_flavor = 'latex'

"increment line numbers, prefix with number of lines to increment
let @i='vykdvp'

" for vhdl convert signal list into port map
let @v='yeea => pa,lD0j'

" for latex change text into equations
let @t='wi\[ $a \]j0'
