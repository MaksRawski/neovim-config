if exists("did_load_filetypes")
	finish
endif
augroup filetypedetect
	au! BufRead,BufNewFile *.cpu setfiletype nasm
	au! BufRead,BufNewFile *.glsl setfiletype glsl
	au! BufRead,BufNewFile *.frag setfiletype glsl
	au! BufRead,BufNewFile *.peg  setfiletype pegjs
	au! BufRead,BufNewFile grub  setfiletype grub

	" folds
	au! BufRead,BufNewFile *.json set foldmethod=syntax
augroup END
