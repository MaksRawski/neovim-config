" movement
" Use <C-l> for trigger snippet expand.
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" coq
" todo: remap `cG to go to EOF and run CoqToline$

" next goals
map <leader>]c <buffer><Plug>(CoqGotoGoalNextStart)
map <leader>]C <buffer><Plug>(CoqGotoGoalNextEnd)

" previous goals
map <leader>[c <buffer><Plug>(CoqGotoGoalPrevStart)
map <leader>[c <buffer><Plug>(CoqGotoGoalPrev)
map <leader>[C <buffer><Plug>(CoqGotoGoalPrevEnd)
map <leader>[C <buffer><Plug>(CoqGotoGoalPrev!)

" gitgutter
nmap ghp <Plug>(GitGutterPreviewHunk)
nmap ghu <Plug>(GitGutterUndoHunk)
nmap ghs <Plug>(GitGutterStageHunk)
xmap ghs <Plug>(GitGutterStageHunk)

map <F1> <Esc>
imap <F1> <Esc>
nnoremap <F2> <cmd>CHADopen<cr>
nnoremap <F5> :UndotreeToggle<cr>
map <F8> :TagbarToggle<CR>

nnoremap <leader>b :Buffers<CR>
nnoremap <leader>e :FZF<CR>

map zf <Plug>(incsearch-fuzzyspell-/)

nmap <silent> <leader>h :noh<CR>

nmap <silent> gb :b#<CR>
nmap <silent> gB :brewind<CR>

nmap <leader>ds :DiffSaved<CR>
nmap <leader>m :FixFormatting<CR>
nmap <leader>f :Leaderf rg --popup<CR>
nnoremap <leader>m :call FixFormatting()<CR>

" map <leader>p silent !pandoc % -o %:r.pdf &<CR>
" map leader o open eg. pdf in latex
map <leader>o:!okular %:r.pdf &<CR>
" tnoremap <Esc> <C-\><C-N>
" nnoremap <Leader>t :terminal<CR>

" autocommands
" latex
au! BufRead *.tex silent !pdflatex '%' && zathura %:r.pdf &
au! BufWritePost *.tex silent !pdflatex '%' &
au! ExitPre *.tex silent !killall zathura; rm *.log *.aux %:r.pdf &

" markdown
au! BufRead *.md silent !pandoc '%' -o %:r.pdf & zathura %:r.pdf &
au! BufWritePost *.md silent !pandoc '%' -o %:r.pdf &
au! ExitPre *.md silent !killall zathura; rm *.log *.aux %:r.pdf &

"Go to nVim config
nmap <silent>gv :tabe $HOME/.config/nvim/init.vim<CR>
silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)
